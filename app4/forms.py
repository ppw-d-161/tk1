from django import forms
from app4.models import MessageAndExpectation


class MessageAndExpectationForm(forms.ModelForm):
    class Meta:
        model = MessageAndExpectation
        fields = (
            'name',
            'message',
        )

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'message': forms.Textarea(attrs={'class': 'form-control'}),
        }
