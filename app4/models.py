from django.db import models

# Create your models here.
class MessageAndExpectation(models.Model):
    name = models.CharField(max_length=50)
    message = models.TextField()
