from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from .models import MessageAndExpectation
from .forms import MessageAndExpectationForm
from .views import message, confirmation, add_message, prevention


class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.message = MessageAndExpectation.objects.create(name="faris", message="i hope that our lives back to normal soon")
        self.index = reverse("message:index")
        self.confirmation = reverse("message:confirmation")
        self.add_message = reverse("message:add")
        self.prevention = reverse("message:prevention")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "message.html")

    def test_POST_confirmation(self):
        response = self.client.post(self.confirmation,
                                    {
                                        'name': 'faris',
                                        'message': 'I hope a vaccine is found soon, and everyone’s life goes back to normal'
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "confirmation.html")
        self.assertContains(response, "faris")

    def test_POST_add_message(self):
        response = self.client.post(self.add_message,
                                    {
                                        'name': 'faris',
                                        'message': 'I hope a vaccine is found soon, and everyone’s life goes back to normal'
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "I hope a vaccine is found soon, and everyone’s life goes back to normal")

    def test_GET_confirmation(self):
        response = self.client.get(self.confirmation, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "message.html")

    def test_GET_add_message(self):
        response = self.client.get(self.add_message, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "message.html")

    def test_GET_prevention(self):
        response = self.client.get(self.prevention, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "prevention.html")
