from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import inlineformset_factory, modelformset_factory
from app4.forms import MessageAndExpectationForm
from app4.models import MessageAndExpectation


# Create your views here.
def message(request):
    message = MessageAndExpectation.objects.all()
    form = MessageAndExpectationForm()
    context = {
        'message': message,
        'form': form,
    }
    return render(request, 'message.html', context)

def confirmation(request):
    if request.method == "POST":
        form = MessageAndExpectationForm(request.POST)
        context = {
            'form': form,
            'name': request.POST['name'],
            'message': request.POST['message']
        }
        return render(request, 'confirmation.html', context)
    else:
        return redirect('message:index')

def add_message(request):
    if request.method == "POST":
        form = MessageAndExpectationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('message:index')
    else:
        return redirect('message:index')

def prevention(request):
    return render(request, 'prevention.html')



