from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'message'
#url for app, add your URL Configuration

urlpatterns = [
    path('', views.message, name='index'),
    path('confirmation/', views.confirmation, name='confirmation'),
    path('confirmation/add/', views.add_message, name='add'),
    path('prevention/', views.prevention, name='prevention'),
]