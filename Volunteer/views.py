from django.shortcuts import render, redirect
from .models import Volunteer as donator
from .forms import VolunteerForm

def Join(request):
    form = VolunteerForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            volun = donator()
            volun.Nama = form.cleaned_data['Nama']
            volun.Tujuan = form.cleaned_data['Tujuan']
            volun.Kontak = form.cleaned_data['Kontak']
            volun.save()
            return redirect('/Volunteer')
    
    volun = donator.objects.all()
    form = VolunteerForm()
    response = {"volun":volun, 'form' : form}
    return render(request,'Volunteer.html',response)

# def volun_delete(request, pk):
#     form = VolunteerForm(request.POST)
#     if request.method == "POST":
#         if form.is_valid():
#             volun = donator()
#             volun.Nama = form.cleaned_data['Nama']
#             volun.Tujuan = form.cleaned_data['Tujuan']
#             volun.Kontak = form.cleaned_data['Kontak']
#             volun.save()
#             return redirect('/Volunteer')
    
#     donator.objects.filter(pk=pk).delete()
#     data = donator.objects.all()
#     form = VolunteerForm()
#     response = {"volun":data, 'form' : form}
#     return render(request, 'Volunteer.html', response)

def detail(request, pk):
    volun = donator.objects.all().filter(id=pk)
    response = {"VolunteerForm" : volun}
    return render(request, "detail.html", response)

def about(request):
    return render(request, 'About.html')
    