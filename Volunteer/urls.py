from django.urls import path
from .views import Join, detail
from . import views

app_name = 'volunteer'

urlpatterns = [
    path('', views.Join, name='Volunteer'),
    path('detail/<int:pk>', views.detail, name ='Detail'),
    path('About/', views.about, name='About'),
]