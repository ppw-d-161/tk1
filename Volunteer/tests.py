from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from . import models
from .views import Join, detail
from .forms import VolunteerForm
from django.apps import apps
from .apps import VolunteerConfig

# Create your tests here.

class TestURL(TestCase):

    def test_volunteer_url1(self):
        response = Client().get('/detail')
        self.assertEquals(response.status_code, 404)

    def test_volunteer_url2(self):
        response = Client().get('/Volunteer/detail')
        self.assertEquals(response.status_code, 404)
    
    def test_statistic_views(self):
        found = resolve('/Volunteer/')
        self.assertEqual(found.func, Join)
    
    def setUp(self):
        self.client = Client()

    def test_url_yes(self):
        response = Client().get('/Volunteer')
        self.assertEquals(response.status_code, 301)

    def test_form_save_a_POST_request(self):
        response = Client().post('/Volunteer/', data={'Nama':'rey','Tujuan':'volunteer','Kontak':'08127392900'})
        jumlah = models.Volunteer.objects.filter(Nama='rey').count()
        self.assertEqual(jumlah,1)
    
    def test_form_save_a_POST_request_2(self):
        response = Client().post('/Volunteer/', data={'Nama':'rey','Tujuan':'volunteer','Kontak':'08127392900'})
        jumlah = models.Volunteer.objects.filter(Nama='rey').count()
    
    def test_coba_detail(self):
        self.rey = models.Volunteer.objects.create(Nama='rey',Tujuan='volunteer',Kontak='08127392900')
        jumlah = models.Volunteer.objects.filter(Nama='rey').count()
        self.assertEqual(jumlah,1)

    def test_kasus_url_is_resolved3(self):
        #self.Nama = models.Volunteer.objects.create(Nama='rey',Tujuan='volunteer',Kontak='08127392900')
        response = Client().get('/Volunteer/detail/1')
        self.assertEquals(response.status_code, 200)

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(VolunteerConfig.name, 'Volunteer') 
        self.assertEqual(apps.get_app_config('Volunteer').name, 'Volunteer')
