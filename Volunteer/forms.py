from django import forms

class VolunteerForm(forms.Form):

    Nama = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Masukkan Nama',
        'type' : 'text',
        'required': True,
    }))

    Tujuan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Volunteer atau Membutuhkan',
        'type' : 'text',
        'required': True,
    }))

    Kontak = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Masukkan Kontak',
        'type' : 'text',
        'required': True,
    }))