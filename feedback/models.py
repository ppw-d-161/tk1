from django.db import models

# Create your models here.

class Feedback(models.Model):
    email = models.CharField(max_length=100)
    kritik_saran = models.TextField()

    def __str__(self):
        return f'{self.email}, {self.kritik_saran}'