from django import forms

#import model dari models.py
from .models import Feedback

class FormFeedback(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = [
            'email',
            'kritik_saran'
        ]

        labels = {
            'email' : 'Email',
            'kritik_saran' : 'Kritik/Saran',
        }

        widgets = {
            'email' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                }
            ),

            'kritik_saran' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                }
            ),
        }