from django.contrib import admin
from django.conf.urls import url
from django.urls import include, path
from . import views
app_name = 'feedback'

urlpatterns = [
    path('', views.create_feedback, name='feedback')
]