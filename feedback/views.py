from django.shortcuts import render, redirect
from .forms import FormFeedback
from .models import Feedback
from django.http import HttpResponseRedirect

# Create your views here.

def create_feedback(request):
    form_feedback = FormFeedback(request.POST or None)

    if request.method == "POST":
        if form_feedback.is_valid():
            form_feedback.save()

            return HttpResponseRedirect('/feedback')
    
    feedback_list = Feedback.objects.all()

    context = {
        'FormFeedback' : form_feedback,
        'page_title' : 'Feedback',
        'feedback_list' : feedback_list
    }
    return render(request, "feedback.html", context)