from django.test import TestCase
from django.test import TestCase , Client
from django.urls import resolve
from .models import Feedback
from . views import create_feedback
# Create your tests here.

class feedbackTest(TestCase):
   
    #test database
    def setUp(self):
        self.create_feedback = Feedback.objects.create(email="nabilaazzahra565@gmail.com", kritik_saran="tingkatkan")

    def test_check_amount_databases(self):
        self.assertEqual(len(Feedback.objects.all()), 1)

    #test url
    def test_info_all_url(self):
        response = self.client.post('/feedback/',{})
        self.assertEqual(response.status_code,200)


    #test views
    def test_info_all_views(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, create_feedback)
    
    #test templates
    def test_info_all_templates(self):
        response = Client().get('/feedback/')
        self.assertTemplateUsed(response,'feedback.html')
    



