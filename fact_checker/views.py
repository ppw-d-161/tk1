from django.shortcuts import render, redirect
from .models import FactCheck
from .forms import FactCheckForm

# Create your views here.
def info_all(request,check=0):
    unfiltered_fact_check = FactCheck.objects.all()
    confirmed = []
    unconfirmed = []
    confirmation = ""
    for i in unfiltered_fact_check:
        if i.is_checked:
            confirmed.append(i)
        else:
            unconfirmed.append(i)

    if check == 1:
        confirmation = "Your report has been submitted!"

    context = {
                'confirmed': confirmed,
                'unconfirmed' : unconfirmed,
                'confirmation':confirmation
    }

    return render(request, 'info_all.html', context)

def info_detail(request,id):
    obj = FactCheck.objects.get(id=id)
    context = {'info' : obj}
    return render(request, 'info_detail.html',context)

def info_create(request):
    form = FactCheckForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('fact_checker:info-all',check = 1)
    context = {'form':form}
    return render(request, 'info_create.html', context)
