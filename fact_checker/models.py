from django.db import models

# Create your models here.
class FactCheck(models.Model):
    # User can Access
    title = models.CharField(max_length=100)
    user_name = models.CharField(max_length=100)
    user_info = models.TextField()

    # Admin only
    is_checked = models.BooleanField(default=False)
    clarification = models.TextField()

    def __str__(self):
        return self.title

