from django.test import TestCase , Client
from django.urls import resolve
from .models import FactCheck
from .views import info_all , info_detail, info_create

# Create your tests here.

class FactCheckTest(TestCase):
   # Database check
    def setUp(self):
        self.fact = FactCheck.objects.create(title='Hoax', user_name="person1", user_info = "there is a hoax")

    def test_check_amount_databases(self):
        self.assertEqual(len(FactCheck.objects.all()), 1)

    def test_check_tostring(self):
        obj = FactCheck.objects.get(pk=1)
        self.assertEqual(str(obj), "Hoax")

    # Checking URLs
    def test_info_all_url(self):
        response = Client().get('/Fact-Checker/')
        self.assertEqual(response.status_code,200)

    def test_info_detail_url(self):
        response = Client().get('/Fact-Checker/detail/1/')
        self.assertEqual(response.status_code,200)

    def test_info_create_url(self):
        response = Client().get('/Fact-Checker/create/')
        self.assertEqual(response.status_code,200)

    # Checking Views
    def test_info_all_views(self):
        found = resolve('/Fact-Checker/')
        self.assertEqual(found.func, info_all)

    def test_info_detail_views(self):
        found = resolve('/Fact-Checker/detail/1/')
        self.assertEqual(found.func, info_detail)

    def test_info_create_views(self):
        found = resolve('/Fact-Checker/create/')
        self.assertEqual(found.func, info_create)

    # Checking Templates
    def test_info_all_templates(self):
        response = Client().get('/Fact-Checker/')
        self.assertTemplateUsed(response,'info_all.html')

    def test_info_detail_templates(self):
        response = Client().get('/Fact-Checker/detail/1/')
        self.assertTemplateUsed(response,'info_detail.html')

    def test_info_create_templates(self):
        response = Client().get('/Fact-Checker/create/')
        self.assertTemplateUsed(response,'info_create.html')

    # Checking logic
    def test_if_HTML_display_models(self):
        response = Client().get('/Fact-Checker/')
        html_response = response.content.decode('utf8')
        self.assertIn("Hoax",html_response)
        self.assertIn("there is a hoax" , html_response)

    # Checking Forms
    def test_saving_a_POST_FactCheckerForm(self):
        response = Client().post('/Fact-Checker/create/', data={'title' :'Hoax2', 'user_name':"person2", 'user_info' : "there is a hoax"})
        amount = FactCheck.objects.filter(title="Hoax2").count()
        self.assertEqual(amount, 1)