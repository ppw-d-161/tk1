from django.apps import AppConfig


class FactCheckerConfig(AppConfig):
    name = 'fact_checker'
