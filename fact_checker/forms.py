from django.db.models import fields
from django import forms
from django.forms import ModelForm, Textarea, TextInput, DateInput
from django.forms import widgets
from .models import FactCheck

class FactCheckForm(ModelForm):
    class Meta:
        model = FactCheck
        fields = ['title','user_name','user_info']
        widgets = {
            'title': TextInput(attrs={'placeholder': 'ex: Hoax on social media', 'required': True}),
            'user_name': TextInput(attrs={'placeholder': 'ex: John Smith', 'required': True}),
            'user_info': TextInput(attrs={'placeholder': 'Fill out your information here', 'required': True}),
            }