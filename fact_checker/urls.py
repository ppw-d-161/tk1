from django.urls import path
from .views import info_all, info_detail, info_create
#url for app
app_name = 'fact_checker'

urlpatterns = [
    path('<int:check>', info_all, name='info-all'),
    path('', info_all, name='info-all'),
    path('detail/<int:id>/', info_detail, name='info-detail'),
    path('create/', info_create, name='info-create')
]