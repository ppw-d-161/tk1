from django.apps import AppConfig


class CovidStatisticConfig(AppConfig):
    name = 'covid_statistic'
