from django.urls import path
from .views import statistic_views, statistic_prov_views
#url for app

app_name = 'stats'
urlpatterns = [
    path('statistic_prov/', statistic_prov_views, name='stats_prov'),
]