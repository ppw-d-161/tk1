from django.shortcuts import render
import urllib.request, json
# Create your views here.

def statistic_views(request):
    with urllib.request.urlopen("https://data.covid19.go.id/public/api/update.json") as url:
        global postive_case
        global recovered
        global deaths
        global last_updated
        data = json.loads(url.read().decode())

        postive_case = data['update']['total']['jumlah_positif']
        recovered = data['update']['total']['jumlah_sembuh']
        deaths = data['update']['total']['jumlah_meninggal']
        last_updated = data['update']['penambahan']['tanggal']

    context = {'postive_case': postive_case, 'recovered':recovered, 'deaths':deaths, 'last_updated':last_updated}
    return render(request, 'statistic.html',context)

def statistic_prov_views(request):
    with urllib.request.urlopen("https://data.covid19.go.id/public/api/prov.json") as url:
        global last_updated
        global data_covid

        data = json.loads(url.read().decode())

        last_updated = data["last_date"]
        data_covid = data["list_data"]

    context = {'last_updated': last_updated , 'data_covid': data_covid}
    return render(request, 'statistic_prov.html', context)